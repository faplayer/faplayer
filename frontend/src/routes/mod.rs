use yew_router::prelude::*;
use yew_router::switch::Permissive;

/// App routes
#[derive(Switch, Debug, Clone)]
pub enum AppRoute {
    #[to = "/#connect_to_spotify"]
    Login,
    #[to = "/#settings"]
    Settings,
    #[to = "/#page-not-found"]
    PageNotFound(Permissive<String>),
    #[to = "/"]
    Home,
}

