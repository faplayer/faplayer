use lazy_static::lazy_static;
use log::debug;
use parking_lot::RwLock;
use serde::{Deserialize, Serialize};
use yew::callback::Callback;
use yew::format::{Json, Nothing, Text};
use yew::services::fetch::{FetchService, FetchTask, Request, Response};
use yew::services::storage::{Area, StorageService};
use serde_json;
use crate::error::ThisError;
// use anyhow::Error;
use shared_types::ErrorInfo;


const API_ROOT: &str = "/api/v1/";
const TOKEN_KEY: &str = "faplayer.token";

lazy_static! {
    /// Jwt token read from local storage.
    pub static ref TOKEN: RwLock<Option<String>> = {
        let storage = StorageService::new(Area::Local).expect("storage was disabled by the user");
        if let Ok(token) = storage.restore(TOKEN_KEY) {
            RwLock::new(Some(token))
        } else {
            RwLock::new(None)
        }
    };
}

pub fn set_token(token: Option<String>) {
    let mut storage = StorageService::new(Area::Local).expect("storage was disabled by the user");
    if let Some(t) = token.clone() {
        storage.store(TOKEN_KEY, Ok(t));
    } else {
        storage.remove(TOKEN_KEY);
    }
    let mut token_lock = TOKEN.write();
    *token_lock = token;
}

pub fn get_token() -> Option<String> {
    let token_lock = TOKEN.read();
    token_lock.clone()
}

pub fn is_authenticated() -> bool {
    get_token().is_some()
}

/// Http request
#[derive(Default, Debug)]
pub struct Requests {}

impl Requests {
    pub fn new() -> Self {
        Self {}
    }

    pub fn builder<B, T>(
        &mut self,
        method: &str,
        url: String,
        body: B,
        callback: Callback<Result<T, ThisError>>,
    ) -> FetchTask
        where
                for<'de> T: Deserialize<'de> + 'static + std::fmt::Debug,
                B: Into<Text> + std::fmt::Debug,
    {
        let handler = move |response: Response<Text>| {
            if let (meta, Ok(data)) = response.into_parts() {
                debug!("Response: {:?}", data);
                if meta.status.is_success() {
                    let data: Result<T, _> = serde_json::from_str(&data);
                    if let Ok(data) = data {
                        callback.emit(Ok(data))
                    } else {
                        callback.emit(Err(ThisError::DeserializeError))
                    }
                } else {
                    match meta.status.as_u16() {
                        401 => callback.emit(Err(ThisError::Unauthorized)),
                        403 => callback.emit(Err(ThisError::Forbidden)),
                        404 => callback.emit(Err(ThisError::NotFound)),
                        500 => callback.emit(Err(ThisError::InternalServerError)),
                        422 => {
                            let data: Result<ErrorInfo, _> = serde_json::from_str(&data);
                            if let Ok(data) = data {
                                callback.emit(Err(ThisError::UnprocessableEntity(data)))
                            } else {
                                callback.emit(Err(ThisError::DeserializeError))
                            }
                        }
                        _ => callback.emit(Err(ThisError::RequestError)),
                    }
                }
            } else {
                callback.emit(Err(ThisError::RequestError))
            }
        };

        let url = format!("{}{}", API_ROOT, url);
        let mut builder = Request::builder()
            .method(method)
            .uri(url.as_str())
            .header("Content-Type", "application/json");
        if let Some(token) = get_token() {
            builder = builder.header("Authorization", format!("Token {}", token));
        }
        let request = builder.body(body).unwrap();
        debug!("Request: {:?}", request);

        FetchService::fetch(request, handler.into()).unwrap()
    }

    /// Delete request
    pub fn _delete<T>(&mut self, url: String, callback: Callback<Result<T, ThisError>>) -> FetchTask
        where
                for<'de> T: Deserialize<'de> + 'static + std::fmt::Debug,
    {
        self.builder("DELETE", url, Nothing, callback)
    }

    /// Get request
    pub fn get<T>(&mut self, url: String, callback: Callback<Result<T, ThisError>>) -> FetchTask
        where
                for<'de> T: Deserialize<'de> + 'static + std::fmt::Debug,
    {
        self.builder("GET", url, Nothing, callback)
    }

    /// Post request with a body
    pub fn _post<B, T>(
        &mut self,
        url: String,
        body: B,
        callback: Callback<Result<T, ThisError>>,
    ) -> FetchTask
        where
                for<'de> T: Deserialize<'de> + 'static + std::fmt::Debug,
                B: Serialize,
    {
        let body: Text = Json(&body).into();
        self.builder("POST", url, body, callback)
    }

    /// Put request with a body
    pub fn _put<B, T>(
        &mut self,
        url: String,
        body: B,
        callback: Callback<Result<T, ThisError>>,
    ) -> FetchTask
        where
                for<'de> T: Deserialize<'de> + 'static + std::fmt::Debug,
                B: Serialize,
    {
        let body: Text = Json(&body).into();
        self.builder("PUT", url, body, callback)
    }
}
