use yew::callback::Callback;
use yew::services::fetch::FetchTask;

use super::requests::Requests;
use crate::error::ThisError;
use shared_types::IndexApiResponse;

#[derive(Default, Debug)]
pub struct Auth {
    requests: Requests,
}

impl Auth {
    pub fn new() -> Self {
        Self {
            requests: Requests::new(),
        }
    }

    /// Get current info
    pub fn current(&mut self, callback: Callback<Result<IndexApiResponse, ThisError>>) -> FetchTask {
        self.requests
            .get::<IndexApiResponse>("index".to_string(), callback)
    }
}