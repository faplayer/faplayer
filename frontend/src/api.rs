use serde::{Deserialize, Serialize};
use yew::services::fetch::{FetchService, FetchTask, Request, Response};

const API_URL: &'static str = "/api/v1/";

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
pub struct Hello {
    pub spotify_token: Some(String),
}

#[derive(Debug, Default, Clone, Serialize, Deserialize, PartialEq)]
pub struct ApiResponse {
    pub status: String,
    pub data: Vec<Hello>,
}

#[derive(Default, Debug, Clone)]
pub struct Request;

impl FetchRequest for Request {
    type RequestBody = ();
    type ResponseBody = ApiResponse;
    type Format = Json;

    fn url(&self) -> String {
        [API_URL.to_string(), "hello".to_string()].concat()
    }

    fn method(&self) -> MethodBody<Self::RequestBody> {
        MethodBody::Get
    }

    fn headers(&self) -> Vec<(String, String)> {
        vec![]
    }

    fn use_cors(&self) -> bool {
        true
    }
}