use crate::routes::{AppRoute};
use serde_derive::{Deserialize, Serialize};
use shared_types::IndexApiResponse;
use yew::prelude::*;

use yew::services::fetch::{FetchTask};
use yew::{html, Component, ComponentLink, Html, ShouldRender};
use yew_router::prelude::*;

use crate::components::spotify_auth_component::SpotifyAuthComponent;
use crate::services::{is_authenticated, Auth, set_token};
use crate::error::ThisError;
use log::debug;
use yew_router::agent::RouteRequest::ChangeRoute;


#[derive(Serialize, Deserialize)]
pub struct State {
    entries: Vec<Entry>,
    index: Option<IndexApiResponse>,
}

#[derive(Serialize, Deserialize)]
struct Entry {
    description: String,
    completed: bool,
    editing: bool,
}

pub enum Msg {
    CurrentUserResponse(Result<IndexApiResponse, ThisError>),
    Route(Route),
    Authenticated(IndexApiResponse),
}

/// The root app component
pub struct App {
    auth: Auth,
    current_route: Option<AppRoute>,
    current_user: Option<String>,
    current_user_response: Callback<Result<IndexApiResponse, ThisError>>,
    current_user_task: Option<FetchTask>,
    #[allow(unused)]
    router_agent: Box<dyn Bridge<RouteAgent>>,
    link: ComponentLink<Self>,
}


impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let router_agent = RouteAgent::bridge(link.callback(Msg::Route));
        let route_service: RouteService = RouteService::new();
        let route = route_service.get_route();
        // fix_fragment_routes(&mut route);
        App {
            auth: Auth::new(),
            current_route: AppRoute::switch(route),
            router_agent,
            current_user: None,
            current_user_response: link.callback(Msg::CurrentUserResponse),
            current_user_task: None,
            link,
        }
    }


    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::CurrentUserResponse(Ok(user_info)) => {
                debug!("CurrentUserResponse {:?}", user_info);
                self.current_user = user_info.web_token.clone();
                set_token(user_info.web_token.clone());
                self.current_user_task = None;
                if self.current_user.is_none() {
                    self.router_agent.send(ChangeRoute(AppRoute::Login.into()));
                }
            }
            Msg::CurrentUserResponse(Err(_)) => {
                self.current_user_task = None;
            }
            Msg::Route(route) => {
                // fix_fragment_routes(&mut route);
                self.current_route = AppRoute::switch(route)
            }
            Msg::Authenticated(user_info) => {
                self.current_user = user_info.web_token;
            }
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        debug!("{:?}", self.current_user);
        let callback_login = self.link.callback(Msg::Authenticated);

        html! {
        <div class="root">
            <div class="w98 desktop">
            {
            if let Some(route) = &self.current_route {
                match route {
                    AppRoute::Login => {
                        html! {<SpotifyAuthComponent callback=callback_login/>}
                    }
            AppRoute::Home => {
                html! {"HOME"}
            }
            AppRoute::Settings => {
                html! {"Settings"}
            }
            AppRoute::PageNotFound(_) => {html! {"Not Found"}}
                }
            } else {
                        // 404 when route matches no component
                        html! { "No child component available" }
                    }
            }
                <footer class="info">
                    <p>{ "Part of " }<a href="" target="_blank">{ "Faplayer" }</a></p>
                </footer>
            </div>
            <div class="container"><div class="screen"></div></div>
         </div>
        }
    }

    fn rendered(&mut self, first_render: bool) {
        // Get current user info if a token is available when mounted

        // let task = self.auth.current(self.current_user_response.clone());
        // self.current_user_task = Some(task);
        if first_render {
            let task = self.auth.current(self.current_user_response.clone());
            self.current_user_task = Some(task);
        }
    }
}
