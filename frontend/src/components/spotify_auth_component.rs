use crate::routes::{AppRoute};
use yew::prelude::*;
use yew::{Component, ComponentLink};
use yew_router::{agent::RouteRequest::ChangeRoute, prelude::*};
use shared_types::IndexApiResponse;
use crate::error::ThisError;
use yew::services::fetch::FetchTask;
use crate::services::{Auth, get_token};
use log::debug;



pub enum Msg {
    Ignore,
    ToHome,
}

#[derive(PartialEq, Properties, Clone)]
pub struct Props {
    pub callback: Callback<IndexApiResponse>,
}

pub struct SpotifyAuthComponent {
    auth: Auth,
    error: Option<ThisError>,
    task: Option<FetchTask>,
    props: Props,
    router_agent: Box<dyn Bridge<RouteAgent>>,
    link: ComponentLink<Self>,
}

impl SpotifyAuthComponent {
    fn alert_message(&self, tkn: &Option<String>) -> Html {
        // let Props {
        //     ..
        // } = self.props;

        if tkn.is_some() {
            html!{<div>{"Nice! You are connecting to Spotify!!"}</div>}
        }
        else {
            html!{<div>{"Hello! It looks like Spotify is not connected for you. \
            You gotta do it"}</div>}
        }

    }

    fn bottom_buttons(&self, tkn: &Option<String>) -> Html {
        if tkn.is_none() {
            return html! {
            <div class="window--alert__actions">
                <a href="/api/v1/spotify_auth"
                class="btn btn--form window--alert__ok"
                style="text-decoration: none; color: inherit">
                {"Connect to Spotify"}</a>
            </div>
            };
        } else {
            return html! {
            <div class="window--alert__actions">
                <button class="btn btn--form window--alert__cancel"
                   onclick=self.link.callback(|_| Msg::ToHome )>{"Ok"}</button>
            </div>
            };
        };

    }
}

impl Component for SpotifyAuthComponent {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        SpotifyAuthComponent {
            auth: Auth::new(),
            error: None,
            props,
            router_agent: RouteAgent::bridge(link.callback(|_| Msg::Ignore)),
            task: None,
            link,
        }
    }
    fn view(&self) -> Html {
        let title = "Need Spotify Auth";
        // if self.props.has_token {
        //     title = "Auth Success"
        // }
        let tkn = get_token();
        html! {
            <div title={title}
            class="Frame window window--alert IframeWindow--alert window--active">
                <div class="window__heading">
                    <div class="window__title">{title}</div>
                    <button class="btn btn--nav window__close"></button>
                </div>
                <div class="window--alert__message">
                { self.alert_message(&tkn) }
                </div>
                { self.bottom_buttons(&tkn) }
            </div>
        }
    }
    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Ignore => {},
            Msg::ToHome => {
                self.router_agent.send(ChangeRoute(AppRoute::Home.into()));
            },
        }
        true
    }
    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }
}
