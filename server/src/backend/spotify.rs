use rspotify::client::Spotify;
use rspotify::oauth2::{SpotifyClientCredentials, TokenInfo};

use crate::auth::{get_oauth};
use crate::cli::AppConfig;

fn get_spotify(token_info: TokenInfo) -> Spotify {
    let client_credential = SpotifyClientCredentials::default()
        .token_info(token_info)
        .build();
    let spotify = Spotify::default()
        .client_credentials_manager(client_credential)
        .build();
    spotify
}

pub async fn change_play(uri: String, app_config: AppConfig) {
    let mut o = get_oauth(app_config);
    let tok = o.get_cached_token().await;
    if tok.is_none() {
        return;
    }
    // let tok = get_token_auto(&mut o, 8888).await.unwrap();
    let spot = get_spotify(tok.unwrap());
    println!("{:?}", spot.device().await);
    spot.transfer_playback("4350f610a45fe7292ce4a00bdf69392fb17167db", false).await;
    spot.start_playback(None, Some(uri.to_string()), None, None, Some(1000)).await.unwrap();
}

