use clap::Clap;
use crate::backend::spotify::change_play;
use std::fs::File;
use std::fs;
use serde::{Deserialize};
use crate::server::run_server;

pub async fn sptf(uri: String, app_config: AppConfig) -> () {
    println!("{:?}", uri);
    let s = vec![uri, "/spotify".to_string()].concat();
    println!("{:?}", s);
    let f = fs::read_to_string(s).expect("Unable to read file");
    println!("{}", f);
    change_play(f.trim().parse().unwrap(), app_config).await
}

#[derive(Clap)]
#[clap(version = "1.0", author = "Dmitriy Zhiltsov <dmitriy@pushamp.com>")]
pub struct Opts {
    #[clap(short, long, default_value = "/etc/faplayer/faplayer.toml")]
    config: String,
    #[clap(subcommand)]
    subcmd: SubCommand,
}
#[derive(Debug, Deserialize, Clone)]
pub struct SpotifyConfig {
    pub client_id: String,
    pub client_secret: String,
    pub device_name: String,
    pub token_path: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct AppConfig {
    pub spotify: SpotifyConfig
}

impl Opts {
    pub async  fn get_handler(self)  {
        // let app_config = self.get_config().clone();
        match self.subcmd {
            SubCommand::Play(ref t) => sptf(t.path.clone(), self.get_config().clone()).await,
            SubCommand::Server(ref _t) => run_server(self.get_config()).await,
            _ => println!("Hi")
        }
    }

    pub fn get_config(self) -> AppConfig {
        let f = fs::read_to_string(self.config).expect("Unable to read file");
        toml::from_str(&f).unwrap()
    }
}
#[derive(Clap)]
enum SubCommand {
    Test(Test),
    Play(Play),
    Server(Server),
}

/// A subcommand for controlling testing
#[derive(Clap)]
struct Test {
    /// Print debug info
    #[clap(short)]
    debug: bool
}

/// A subcommand for play
#[derive(Clap)]
struct Play {
    /// Print debug info
    #[clap(short)]
    path: String
}

/// A subcommand for serve main daemon
#[derive(Clap)]
struct Server {

}