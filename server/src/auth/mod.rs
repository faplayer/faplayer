mod redirect_uri;

use rspotify::{
    oauth2::{SpotifyOAuth, TokenInfo},
    util::{process_token, request_token},
};
use std::{
    io::{self},
};

use crate::cli::AppConfig;

const SCOPES: [&str; 14] = [
    "playlist-read-collaborative",
    "playlist-read-private",
    "playlist-modify-private",
    "playlist-modify-public",
    "user-follow-read",
    "user-follow-modify",
    "user-library-modify",
    "user-library-read",
    "user-modify-playback-state",
    "user-read-currently-playing",
    "user-read-playback-state",
    "user-read-playback-position",
    "user-read-private",
    "user-read-recently-played",
];
pub fn get_oauth(app_config: AppConfig) -> SpotifyOAuth {
    SpotifyOAuth::default()
        .client_id(&app_config.spotify.client_id)
        .client_secret(&app_config.spotify.client_secret)
        .redirect_uri("http://localhost:8000/api/v1/spotify_callback")
        .cache_path(app_config.spotify.token_path.parse().unwrap())
        .scope(&SCOPES.join(" "))
        .build()
}

