mod auth;
mod cli;
pub mod backend;
mod server;

extern crate rspotify;
use clap::Clap;

use cli::Opts;


#[tokio::main]
async fn main() {
    let opts: Opts = Opts::parse();
    opts.get_handler().await;
}
