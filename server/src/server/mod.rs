use crate::auth::get_oauth;
use crate::cli::{AppConfig, SpotifyConfig};
use rspotify::util::{generate_random_string, process_token};
use tide::prelude::*;
use tide::{Body, Redirect, Request, Response};

#[derive(Debug, Deserialize)]
struct Index {
    has_token: bool,
}

#[derive(Clone)]
struct State {
    config: AppConfig,
}

// Define a new instance of the state.

pub async fn run_server(app_config: AppConfig)
// -> tide::Result<()>
{
    tide::log::start();
    let state = State { config: app_config };
    let mut app = tide::with_state(state);
    // app.with(tide::log::LogMiddleware::new());
    app.at("/").get(index);
    app.at("/api/v1/spotify_callback").get(spotify_callback);
    app.at("/api/v1/spotify_auth").get(spotify_auth);
    app.at("/api/v1/index").get(index_api);
    app.listen("0.0.0.0:8888").await;
    // Ok(())
}

use shared_types::IndexApiResponse;

async fn index(req: Request<State>) -> tide::Result {
    let mut o = get_oauth(req.state().config.clone());
    let token = o.get_cached_token().await;
    let has_token = token.is_some();
    Ok(format!("Hello, you token is : {}", has_token).into())
}

async fn index_api(req: Request<State>) -> tide::Result {
    let mut o = get_oauth(req.state().config.clone());
    let token = o.get_cached_token().await;
    let has_token = token.is_some();
    let mut web_token;
    match token {
        Some(v) => web_token = Some(v.access_token),
        None => web_token = None,
    };
    let h = IndexApiResponse {
        has_token,
        web_token,
    };
    let mut res = Response::new(200);
    res.set_body(Body::from_json(&h)?);
    Ok(res)
}

async fn spotify_auth(req: Request<State>) -> tide::Result {
    let o = get_oauth(req.state().config.clone());
    let state = generate_random_string(16);
    let auth_url = o.get_authorize_url(Some(&state), None);
    Ok(Redirect::new(auth_url).into())
}

async fn spotify_callback(req: Request<State>) -> tide::Result {
    let mut o = get_oauth(req.state().config.clone());
    let mut url = req.url().to_string().clone();
    let token = process_token(&mut o, &mut url).await;
    Ok(Redirect::temporary("/#connect_to_spotify").into())
}
